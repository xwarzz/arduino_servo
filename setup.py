"""Minimal setup file for ul_turntable_package project."""

from setuptools import setup, find_packages

setup(
    name='emcware_dogfood',
    version='0.0.1',
    license='proprietary',
    description='an application to test out ul_emc_packages',

    author='Mike Kriege',
    author_email='mike@emcware.com',
    url='www.emcware.com',

    packages=find_packages(where='src'),
    package_dir={'': 'src'},

    # install_requires=['click', 'tinydb', 'six'],
    install_requires=['serial', 'wxpython'],
    # extras_require={'mongo': 'pymongo'},

    # entry_points={
    #     'console_scripts': [
    #         'emcware_turntable_package_proj = src.emcware_turntable_package:my_turntable',
    #     ]
    # },
)
