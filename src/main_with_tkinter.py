
import functions
import time
from tkinter import *


time.sleep(2)   # sleep for two seconds to establish communication
# print(connection.readline())    # read the serial data from Arduino


#Create the window
win = Tk()

#Modify the root window
win.title("Xwar system") # gives title to window
win.geometry("320x200") # sets the size of the window

#Creating components
f = Frame(win) # creates window

# create some buttons with labels with text
l = Label(win , text = "PANEL CONTROL")
b1 = Button(f, text ="Relays OFF")
b2 = Button(f, text ="Relays ON")
b3 = Button(f, text ="Servos Bowing")
b4 = Button(f, text ="Servo Moving")


# define methods to assign to the button.
b1.configure(command = functions.but1) # assigning methods to buttons
b2.configure(command = functions.but2) # assigning methods to buttons
b3.configure(command = functions.but3) # assigning methods to buttons
b4.configure(command = functions.but4) # assigning methods to buttons

#Adding Components
l.pack() #packs in the label
b3.pack(side = LEFT)
b1.pack(side = LEFT) #packs the buttons in one after the other
b2.pack(side = LEFT) #packs the buttons in one after the other
b4.pack(side = LEFT)

f.pack()

win.mainloop() # start Gui