# https://stackoverflow.com/questions/40510609/send-udp-packet-to-esp8266-from-python

import socket
import time
from tkinter import *

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

time.sleep(2)   # sleep for two seconds to establish communication
# print(connection.readline())    # read the serial data from Arduino


def send_command(val):
    # print(val)    # print val to make sure we are reading button
    sock.sendto(val.encode(), ("192.168.106.126", 5007)) # encode the bytes (Python 3 thing)
    # connection.close()

#Create the window
win = Tk()

#Modify the root window
win.title("Arduino system") # gives title to window
win.geometry("320x200") # sets the size of the window

#Creating components
f = Frame(win) # creates window

#label with text
l = Label(win , text = "Flash LED")
b1 = Button(f, text ="Send 0")
b2 = Button(f, text ="Send 1")
b3 = Button(f, text ="Paul's Button")


#Defining methods
def but1():
    var = '0'
    send_command('0')  # command run if button 1 pressed
    print(f'command {var} sent')


def but2():
    send_command('1')  # command run if button 1 pressed
    print('command 1 sent')


def but3():
    send_command('10')  # command run if button 1 pressed
    print('commnand 10 sent')



# define methods to assign to the button.
b1.configure(command = but1) # assiging methods to buttons
b2.configure(command = but2) # assiging methods to buttons
b3.configure(command = but3) # assiging methods to buttons


#Adding Components
l.pack() #packs in the label
b3.pack(side = LEFT)
b1.pack(side = LEFT) #packs the buttons in one after the other
b2.pack(side = LEFT) #packs the buttons in one after the other


f.pack()

win.mainloop() # start Gui