# https://stackoverflow.com/questions/40510609/send-udp-packet-to-esp8266-from-python

import socket
import time
from tkinter import *

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

time.sleep(2)   # sleep for two seconds to establish communication
# print(connection.readline())    # read the serial data from Arduino


def send_command(val):
    sock.sendto(val.encode(), ("192.168.106.207", 5007)) # encode the bytes (Python 3 thing)
    # connection.close()
def send_cmd_ip210(val_a):
    sock.sendto(val_a.encode(), ("192.168.106.210", 5008))  # encode the bytes (Python 3 thing)
    # connection.close()
def send_cmd_ip_170(val2):
    sock.sendto(val2.encode(), ("10.0.1.79", 5009))  # encode the bytes (Python 3 thing)
    print(val2)
    # connection.close()

    
#Create the window
win = Tk()

#Modify the root window
win.title("Arduino system") # gives title to window
win.geometry("320x200") # sets the size of the window

#Creating components
f = Frame(win) # creates window

#label with text
l = Label(win , text = "Our really cool Robot Controller")
b1 = Button(f, text ="HelloWorld")
b2 = Button(f, text ="Relays ON")
b3 = Button(f, text ="Bow Twice")
b4 = Button(f, text ="Servo Moving")

#Defining methods
def but1():
    send_cmd_ip_170('Hello World')


def but2(): send_cmd_ip210('ijkl')


def but3():
    send_cmd_ip_170('SERVO_BOWS')
    send_cmd_ip_170('SERVO_BOWS')


def but4():
    send_cmd_ip_170('mnmn')

# define methods to assign to the button.
b1.configure(command = but1) # assiging methods to buttons
b2.configure(command = but2) # assiging methods to buttons
b3.configure(command = but3) # assiging methods to buttons
b4.configure(command = but4) # assiging methods to buttons

#Adding Components
l.pack() #packs in the label
b3.pack(side = LEFT)
b1.pack(side = LEFT) #packs the buttons in one after the other
b2.pack(side = LEFT) #packs the buttons in one after the other
b4.pack(side = LEFT)

f.pack()

win.mainloop() # start Gui