 

void wifiDecoder()
  { 
    // We get the number of available bytes
  int noBytes = Udp.parsePacket();
  
  // We read all of the available bytes into packetBuffer
  if ( noBytes > 0) {
    Serial.print(millis() / 1000);
    Serial.print(":Packet of ");
    Serial.print(noBytes);
    Serial.print(" received from ");
    Serial.print(Udp.remoteIP());
    Serial.print(":");
    Serial.println(Udp.remotePort());
    
    // We've received a packet, read the data from it
    Udp.read(packetBuffer,noBytes); // read the packet into the buffer

    // We initialize our variable 'val2'
    char val2 = '2';  // <= Let's get rid of this and use descriptive commands.

    // Here is how we use descriptive commands.
    // We build a multicharacter string 'my_buffer' from the packetBuffer
    // We initialize my_buffer with an underscore - stupid C++ can't handle empty.
    my_buffer = '_';
    for (int i=1;i<noBytes;i++) {
      my_buffer = my_buffer + char(packetBuffer[i-1]);
    }
    // Now we print our descriptive command
    Serial.print(my_buffer);
    Serial.println();

    if (my_buffer == "_HelloWorld");
      {
        HC12.write("Hello\n");      // Send that data to HC-12
      }
  }
}
