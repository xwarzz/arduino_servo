/*    Arduino Long Range Wireless Communication using HC-12 controlled by a WeMos R1
   Gate Controller
   by Paul Kriege & Mike Kriege - January 15, 2019
*/

#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <WiFiUDP.h>

int status = WL_IDLE_STATUS;
const char* ssid = "xwarext1";      // your network SSID (name)
const char* pass = "xwarxwar";      // your network password
String my_buffer ="_";

unsigned int localPort = 5009;      // local port to listen for UDP packets
byte packetBuffer[512];             // buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;

SoftwareSerial HC12(5, 16);         // HC-12 TX Pin, HC-12 RX Pin

void setup() {
  
  // Open serial communications and wait for port to open:
  Serial.begin(1200);               // Serial port to computer
  HC12.begin(1200);                 // Serial port to HC12
  if (HC12.isListening()) {
    Serial.println();
    Serial.println("HC12 is working");
  } else {
    Serial.println();
    Serial.println("HC12 is NOT working");
  }

  // setting up Station AP
  WiFi.begin(ssid, pass);

  // Wait for connect to AP
  Serial.print("[Connecting]");
  Serial.print(ssid);
  int tries=0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    tries++;
    if (tries > 30){
      break;
    }
  }
  Serial.println();
 printWifiStatus();

  Serial.println("Connected to wifi");
  Serial.print("Udp server started at port ");
  Serial.println(localPort);
  Udp.begin(localPort);
}

void loop() {
  //wifiDecoder();
  while (HC12.available()) {        // If HC-12 has data
    Serial.write(HC12.read());      // Send the data to Serial monitor
  }
  while (Serial.available()) {      // If Serial monitor has data
    HC12.write(Serial.read());      // Send that data to HC-12
  }
}
