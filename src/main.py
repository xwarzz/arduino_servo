import wx
import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send_command_ip_address_79(value):
    sock.sendto(value.encode(), ("10.0.1.79", 5009))  # encode the bytes (Python 3 thing)
    print(f'we called this function sock.sent(val.encode) and sent {value}')


def send_command_ip_address_207(value):
    sock.sendto(value.encode(), ("192.168.106.207", 5007))  # encode the bytes (Python 3 thing)


def send_command_ip_address_210(value):
    sock.sendto(value.encode(), ("192.168.106.210", 5009))  # encode the bytes (Python 3 thing)
    # connection.close()


# # Defining methods
# def button_1(): send_command_ip_address_126('efgh')  # command run if button 1 pressed
#
#
# def but2(): send_command_ip_address_126('ijkl')  # command run if button 2 pressed
#
#
# def but3(): send_command_ip_address_207('xayxbyxcyxzy')  # command run if button 3 pressed
#
#
# def but4(): send_command_ip_address_210('mnmn')  # command run if button 4 pressed
#

class LeftPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id, style=wx.BORDER_SUNKEN)

        '''In our example we have two panels. A left and right panel. 
        The left panel has several buttons. The right panel has one static text. 
        The buttons change the number displayed in the static text. 
        The question is, how do we grab the reference to the static text?'''

        '''The answer is here. Each widget has a parent argument. 
        In our example, parent is a panel on which we display both left and right panels. 
        By calling parent.GetParent() we get reference to the frame widget. 
        The frame widget has reference to the rightPanel. 
        Finally, right panel has reference to the static text widget.'''

        self.text = parent.GetParent().rightPanel.text

        # we create two buttons
        button1 = wx.Button(self, -1, 'Button1', (10, 10))     # 10, 10 are the coordinates of button 1
        button2 = wx.Button(self, -1, 'Button2', (10, 40))     # 10, 40 are the coordinates of button 2
        button3 = wx.Button(self, -1, 'Button3', (10, 70))
        button4 = wx.Button(self, -1, 'Button4', (10, 100))
        button5 = wx.Button(self, -1, 'Button5', (10, 130))
        button6 = wx.Button(self, -1, 'Button6', (10, 160))
        button7 = wx.Button(self, -1, 'Button7', (10, 190))
        button8 = wx.Button(self, -1, 'Button8', (10, 220))
        button9 = wx.Button(self, -1, 'Button9', (10, 250))
        button11 = wx.Button(self, -1, 'Shutdown', (10, 310))

        # now we bind buttons to these functions
        self.Bind(wx.EVT_BUTTON, self.button_1, id=button1.GetId())
        self.Bind(wx.EVT_BUTTON, self.button_2, id=button2.GetId())
        self.Bind(wx.EVT_BUTTON, self.table_stop, id=button3.GetId())
        self.Bind(wx.EVT_BUTTON, self.read_position, id=button4.GetId())
        self.Bind(wx.EVT_BUTTON, self.goto_100, id=button5.GetId())
        self.Bind(wx.EVT_BUTTON, self.goto_200, id=button6.GetId())
        self.Bind(wx.EVT_BUTTON, self.ver, id=button7.GetId())
        self.Bind(wx.EVT_BUTTON, self.hor, id=button8.GetId())
        self.Bind(wx.EVT_BUTTON, self.pol, id=button9.GetId())
        self.Bind(wx.EVT_BUTTON, self.shutdown, id=button11.GetId())

    def event_is_not_used(self, event):
        # function to suppress the 'Parameter event is not used' warning.
        pass

    # noinspection PyMethodMayBeStatic
    def button_1(self, event):
        self.event_is_not_used(event)
        send_command_ip_address_79('efgh')

    def button_2(self, event):
        self.event_is_not_used(event)
        send_command_ip_address_79('abcd')

    def table_stop(self, event):
        pass

    def read_position(self, event):
        pass

    def goto_100(self, event):
        pass

    def goto_200(self, event):
        pass

    def ver(self, event):
        pass

    def hor(self, event):
        pass

    def pol(self, event):
        pass

    def shutdown(self, event):
        quit()


class RightPanel(wx.Panel):
    def __init__(self, parent, id):
        wx.Panel.__init__(self, parent, id, style=wx.BORDER_SUNKEN)
        # we create a text field to display 'value'
        self.text = wx.StaticText(self, -1, '0', (40, 60))


class Communicate(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, size=(680, 400))

        panel = wx.Panel(self, -1)
        self.rightPanel = RightPanel(panel, -1)
        left_panel = LeftPanel(panel, -1)

        '''Notice that the right panel must be defined before the left panel. 
        It is because during the construction of the left panel, 
        we are looking for the static text widget, which is defined in the right panel. 
        Logically, we cannot get reference to non existing widget.'''

        right_panel_something = 5  # Setting this to 5 (it was 1), makes the right panel much bigger than the left.
        h_box = wx.BoxSizer()
        h_box.Add(left_panel, 1, wx.EXPAND | wx.ALL, 5)    # 5 is how many pixels to draw the border from top.
        h_box.Add(self.rightPanel, right_panel_something, wx.EXPAND | wx.ALL, 5)

        panel.SetSizer(h_box)
        self.Centre()
        self.Show(True)


app = wx.App()
Communicate(None, -1, 'widgets communicate')
app.MainLoop()
