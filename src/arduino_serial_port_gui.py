import serial
import time
from tkinter import *

port = 'COM7'
speed = 9600


connection = serial.Serial(port, speed, timeout=0)
time.sleep(2)   # sleep for two seconds to establish communication
print(connection.readline())    # read the serial data from Arduino


def send_command(val):
    # print(val)    # print val to make sure we are reading button
    connection.write(bytes(val.encode('ascii')))    # encode the bytes (Python 3 thing)
    # connection.close()

#Create the window
win = Tk()

#Modify the root window
win.title("Arduino system") # gives title to window
win.geometry("320x200") # sets the size of the window

#Creating components
f = Frame(win) # creates window

#label with text
l = Label(win , text = "Flash LED")
b1 = Button(f, text ="Send 0")
b2 = Button(f, text ="Send 1")
b3 = Button(f, text ="Paul's Button")


#Defining methods
def but1():
    cmd = '0'
    send_command(cmd)  # command run if button 1 pressed
    print(f'sent command {cmd}')

def but2():
    cmd ='1'
    send_command(cmd)  # command run if button 1 pressed
    print(f'sent command {cmd}')


# define methods to assign to the button.
b1.configure(command = but1) # assiging methods to buttons
b2.configure(command = but2) # assiging methods to buttons



#Adding Components
l.pack() #packs in the label
b3.pack(side = LEFT)
b1.pack(side = LEFT) #packs the buttons in one after the other
b2.pack(side = LEFT) #packs the buttons in one after the other


f.pack()

win.mainloop() # start Gui