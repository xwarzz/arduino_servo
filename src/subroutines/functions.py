import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def send_command(val):
    sock.sendto(val.encode(), ("192.168.106.207", 5007)) # encode the bytes (Python 3 thing)


def send_command1(val1):
    sock.sendto(val1.encode(), ("192.168.106.126", 5008))  # encode the bytes (Python 3 thing)


def send_command2(val2):
    sock.sendto(val2.encode(), ("192.168.106.210", 5009))  # encode the bytes (Python 3 thing)
    # connection.close()

# Defining methods
def but1(): send_command1('efgh')  # command run if button 1 pressed

def but2(): send_command1('ijkl')  # command run if button 2 pressed

def but3(): send_command('xayxbyxcyxzy')  # command run if button 3 pressed

def but4(): send_command2('mnmn')  # command run if button 4 pressed
