// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad

// increase the visual detail
$fn = 100;

// my variables
shaft_radius = 2.7;   // 2.5 too tight, 2.75 almost snug, 2.675 tight
base_height = 23;        // change to 20
base_radius = 10;       // change to 10

table_radius = 80;


module complexObject()
{
    difference()
    {
        union()
        {
            // everything in here is added 
            base_cylinder();
            turntable();
        }
        
        union()
        {
             // everything in here is subtracted
            shaft();
        }
    }
}
complexObject();
// base_cylinder();
// my_cylinder();
// shaft_cube();



module base_cylinder()
    // translate([50, width/2, 0])
        cylinder(h=base_height, r1=base_radius, r2=base_radius);


module turntable()
    // translate([50, width/2, 0])
        cylinder(h=5, r1=table_radius, r2=table_radius);

module shaft_cylinder()
    translate([0, 0, -1])
        cylinder(h=90, r1=shaft_radius, r2=shaft_radius);

module shaft_cube()
    translate([2, -2.5, -1])
        cube([2, 5, 30]);

module shaft()
    difference()
    {
        shaft_cylinder();
        shaft_cube();
    }
    // translate([50, width/2, 0])
        
        