// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad

// increase the visual detail
$fn = 100;

// my variables
width = 80;
length = 50;
height = 36;
diameter = 57.0;      // add 0.5 mm to radius (1 mm to diameter) so spindle slides.
spindle_radius = diameter/2;
spindle_y_translate = 70;    //   shift spindle in 'y' direction
rail_radius = 7.25;    // was 8
spindle_to_edge = (width - diameter)/2;   //  (80 - 57.5) / 2
clamp_screw_length = 50;  // clamp screw length
ssl = spindle_radius*1.48;     // spindle_square_length
rail_offset = 14;

module complexObject()
{
    difference()
    {
        union()
        {   // everything in here is added 
            cube(size=[width, length, height], center = false);
            polarity_motor_mount();
            polarity_motor_mount_fillet();
        }
        
        union()
        {
             // everything in here is subtracted
            bearing_shaft_1();
            bearing_shaft_2();
            // spindle_opening();
            
            lead_screw_assembly();
            stepper_hole1();
            stepper_hole2();
            stepper_hole3();
            stepper_hole4();
            stepper_coupler_hole();
        }
    }
}

complexObject();


// spindle_hole();
// bearing_shaft_1();
// bearing_shaft_2();

// spindle_opening();
// screw_hole();
// clamp_screw();
// lead_screw_assembly();
// spindle_square();
// nema_motor_coupler_allowance();
// stepper_hole1();
// stepper_hole2();
// stepper_hole3();
// stepper_hole4();

// polarity_motor_screw();
// polarity_motor_mount_fillet();
// stepper_coupler_hole();
// t_nut();

module lead_screw_assembly(){
    translate([width/2, rail_offset, -1])
        union(){
            lead_screw();
            t_nut();
            spring();
            nema_motor_coupler_allowance();
        }
}

module nema_motor_coupler_allowance()
    translate([0, 0, height-5])
        cylinder(h=7, r1=9, r2=9);

module spring()
    translate([0, 0, 20])
        cylinder(h=22, r1=7, r2=7);


module lead_screw()
    cylinder(h=height+2, r1=4.5, r2=4.5);

module t_nut()
    // 8mm leat T-nut is 12.2 mm x 22 mm => adding 0.8 x 1.0
union(){
    translate([-6.5, -11.5, 20])
        cube([13, 23, 40]);     // t_nut was 10, 20, 40
    translate([0, 0, 9])
        cylinder(h=11, d = 9.75);
    translate([0, 8, 12.1])
        cylinder(h=8, d = 2);
    translate([0, -8, 12.1])
        cylinder(h=8, d = 2);
}

 module screw_hole(){
    rotate([90, 0, 0])
        cylinder(h=clamp_screw_length, r1 = 3, r2 = 3);
 }
    
module screw_hole_stop()
    rotate([90, 0, 0])
        cylinder(h=10, r1 = 5, r2 = 5);
    
module screw_nut(){
    nut_height = 12;
    translate([-nut_height/2, -clamp_screw_length+5, -nut_height/2])
        cube([25, 5, 12]);
}

module screw_nut2(){
    nut_height = 12;
    translate([-18, -clamp_screw_length+5, -nut_height/2])
        cube([25, 5, 12]);
}

module spindle_opening()
    // translate([width-20, spindle_y_translate, -1])
    translate([-1, spindle_y_translate, -1])
        cube([150, 5, 45]);


module bearing_shaft_2()
    translate([width/2 + 20, rail_offset, -1])
        rail();

module bearing_shaft_1()
    translate([width/2 - 20, rail_offset, -1])
        rail();

module rail()
    cylinder(h=45, r1 = rail_radius, r2 = rail_radius);
     

module polarity_motor_screw()
    rotate([90, 0, 0])
    union(){
        cylinder(h=20, r1 = 2, r2= 2);
        cylinder(h=3, r1 = 4, r2= 4);
    }

    
module polarity_motor_mount_fillet()
    translate([70, 50, 46])
    rotate([90, 90, 0])
    difference(){
        cube([10, 10, 10]);
        translate([0, 0, -0.01])
            cylinder(h=10.1, r1 = 10, r2= 10);
    }
    
module polarity_motor_mount()
    translate([80, 40, 0])
        cube(size=[50, 10, 50], center = false);



module stepper_hole1()
    translate([89.5, 58, 9.5])
        stepper_hole();
        
module stepper_hole2()
    translate([120.5, 58, 9.5])
        stepper_hole();
        
module stepper_hole3()
    translate([120.5, 58, 40.5])
        stepper_hole(); 

module stepper_hole4()
    translate([89.5, 58, 40.5])
        stepper_hole();

module stepper_hole(){
    rotate([90, 90, 0])
        cylinder(h=20, r1 = 1.75, r2 = 1.75);
    // we add another larger cylinder for the screw heads
        rotate([90, 90, 0])
            cylinder(h=10, r1 = 3, r2 = 3);
}

module stepper_coupler_hole()
    translate([105, 55, 25])
    // {
        // rotate([90, 90, 0])
            // cylinder(h=20, r1 = 8, r2 = 8);
        // translate([0, -12.5, 0])
            rotate([90, 90, 0])
                cylinder(h=20, r1 = 12, r2 = 12);
    //}