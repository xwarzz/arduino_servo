// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad

// increase the visual detail
$fn = 100;

// my variables
shaft_radius = 2.7;   // 2.5 too tight, 2.75 almost snug, 2.675 tight
base_height = 23;        // change to 20
base_radius = 10;       // change to 10

table_radius = 30;

ant_slot_len = 50;
ant_slot_width = 27;
ant_slot_height = 13.5;

ss_radius = 1.1;       // make this slightly smaller than 1.25


module complexObject()
{
    difference()
    {
        union()
        {
            // everything in here is added 
            base_cylinder();
            antenna();
        }
        
        union()
        {
             // everything in here is subtracted
            shaft();
            antenna_slot();
            antenna_barrel_slot();
            set_screw();
            set_screw_2();
            // set_screw_entry();
        }
    }
}

complexObject();
// base_cylinder();
// my_cylinder();
// shaft_cube();
// antenna_barrel_slot();
// set_screw();
// set_screw_entry();

module set_screw_entry()
    translate([9, 0, 15])         // Originally was 14
    rotate([0, 90, 0])
        cylinder(h=2, r1=ss_radius, r2=ss_radius+1);


module set_screw()
    translate([0, 0, 15])         // Originally was 14
    rotate([0, 90, 0])
        cylinder(h=base_height, r1=ss_radius, r2=ss_radius);

module set_screw_2()
    translate([0, 0, -12])
    rotate([0, 90, 0])
        cylinder(h=base_height, r1=ss_radius, r2=ss_radius);

module antenna_slot()
    translate([-ant_slot_width/2, -ant_slot_height/2, -ant_slot_len-3])
        cube([ant_slot_width, ant_slot_height, ant_slot_len]);


module antenna_barrel_slot()
    translate([-8, 6, -ant_slot_len-3])
        cube([16, 8, ant_slot_len]);



module base_cylinder()
union()
    cylinder(h=base_height, r1=base_radius, r2=base_radius);
    klam_fillet();


module antenna()
    translate([-(ant_slot_width+6)/2, -(ant_slot_height+12)/2, -ant_slot_len])
        cube([ant_slot_width+6, ant_slot_height+12, ant_slot_len]);

module shaft_cylinder()
    translate([0, 0, -1])
        cylinder(h=90, r1=shaft_radius, r2=shaft_radius);

module shaft_cube()
    translate([2, -2.5, -1])
        cube([2, 5, 30]);

module shaft()
    difference()
    {
        shaft_cylinder();
        shaft_cube();
    }



module klam_fillet(
    cylinder_height=3,
    cylinder_radius=10,
    fillet_radius_bottom=3,
    fillet_radius_top=0,
    nfaces=50
) {
    /* created by Kevin Lam on Dec 3, 2016 */
    union() {      
        cylinder(cylinder_height, r=cylinder_radius, $fn=nfaces, false);
        
        if (fillet_radius_bottom > 0) {
            difference() {
                cylinder(fillet_radius_bottom, r=cylinder_radius+fillet_radius_bottom, $fn=nfaces, false);
                translate([0, 0, fillet_radius_bottom])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_bottom, 0, 0])
                circle(fillet_radius_bottom, $fn=nfaces);
            }
        }
        
        if (fillet_radius_top>0) {
            difference() {
                translate([0,0,cylinder_height-fillet_radius_top])
                cylinder(fillet_radius_top, r=cylinder_radius+fillet_radius_top, $fn=nfaces, false);
                
                translate([0, 0, cylinder_height-fillet_radius_top])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_top, 0, 0])
                circle(fillet_radius_top, $fn=nfaces);
            }
        }
    }
}
