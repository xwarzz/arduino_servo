// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad

// increase the visual detail
$fn = 100;

// my variables
length = 250;       // x
width = 240;         // y
height = 3;        // z
wall_height = 62.5;

ps_mount1_x = 91.5;
ps_mount1_y = 18.5;

uno_mount1_x = 33.97;
uno_mount1_y = 126;

stepr_mt1_x = 140;
stepr_mt1_y = 125;
stepper_spacing = 31;
hss = stepper_spacing / 2;  // half stepper spacing

table_radius = 80;

shift = 100;
extension_length = 99.5 + shift;  // + shift

        
// enclosure_bottom();
// enclosure_cover();
extension();
// extension_cover();
// thread_formed_screw();
// thread_formed_screw_y_plus();
// thread_formed_screw_y_left();
// uno_board_mount_bracket();

// bottom_hole_and_screwhead();
// stepper_hole_1();

end_wall_w_vent_holes();
// end_wall_drill_template();
// ac_recepticle_cover();

module enclosure_cover()
difference(){
        union(){
        translate([0, 0, 62.5])
            cube([250, 240, 5]);  // lid
        // enclosure_cover_holes();
        }
        enclosure_cover_holes();
    }

module extension_cover()
    
    difference(){
        union(){
        translate([-205.5, 0, 62.5])
            cube([205.5, 240, 5]);  // lid
        translate([-140, 175, 3])
            cube([50, 5, 59.5]);    // vertical post
        translate([-25, 60, 3])
            cube([5, 50, 59.5]);    // vertical post
        }
        turntable();
        extension_cover_holes();
    }
    
module enclosure_cover_holes()
    union(){
        translate([244, 6, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([6, 6, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([244, 233, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([6, 233, 60])
            cylinder(h=10, d=2.5);  // screw hole
    }
    
module turntable()
    translate([1-shift - hss, 60 + 12 + hss, 62.5])
    union(){
        cylinder(h=5.1, r1=table_radius+1, r2=table_radius+1);
            // klam_fillet_cylinder_ps_pad();
    }

module extension_cover_holes()
    union(){
        translate([-6, 6, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([-199, 6, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([-6, 233, 60])
            cylinder(h=10, d=2.5);  // screw hole
        translate([-199, 233, 60])
            cylinder(h=10, d=2.5);  // screw hole
    }

module ac_recepticle_cover()
    union(){
        translate([0, 30, 25])
            cube([40, 3, 34]);  // short side wall
        translate([0, 84, 15])
            cube([40, 3, 44]);  // long side wall
        translate([0, 30, 59])
            cube([40, 57, 3]);  // top wall
        translate([40, 30, 15])
            cube([3, 57, 47]);
    }
        

// end_wall_fan_ac_outlet();


module fan__mounting_holes()
union(){

translate([-5, 200, 13.5])
rotate([90, 270, 90])
    cylinder(h=20, d=2.5);
    
translate([-5, 200-40, 13.5])
rotate([90, 270, 90])
    cylinder(h=20, d=2.5);
    
translate([-5, 200, 13.5+40])
rotate([90, 270, 90])
    cylinder(h=20, d=2.5);
    
translate([-5, 200-40, 13.5+40])
rotate([90, 270, 90])
    cylinder(h=20, d=2.5);
}


module ac_outlet_mounting_holes()
union(){
translate([-6, 38.5, 31.25])
    rotate([90, 270, 90])
        cylinder(h=20, d=2.25);     // ac outlet mounting hole
    
translate([-6, 38.5+40, 31.25])
rotate([90, 270, 90])
    cylinder(h=20, d=2.25);         // ac outlet mounting hole
}
            
module end_wall_fan_ac_outlet()
difference(){
    union(){
        end_wall();
        translate([0, 152, 10])
            cube([9, 50, 10]);  // fan mount bottom ribb
        translate([0, 152, 49])
            cube([9, 50, 10]);  // fan mount top ribb
        translate([0, 152, 9])
            cube([9, 15, 50]);  // fan mount right ribb
        translate([0, 192, 9])
            cube([9, 15, 50]);  // fan mount left ribb
        translate([0, 33.5, 9])
            cube([9, 50, 50]);  // ac outlet flange
        //translate([-13, 40, 0])
            //cube([5, 40, 62.5]);  // fan mount left ribb
    }
    union(){
    fan__mounting_holes();
    translate([-10, 200-20, 13.5+20])
    rotate([90, 270, 90])
        cylinder(h=20, d=45);   // fan air hole
    translate([-10, 45, 7.25])
            cube([40, 28, 48]); // ac outlet hole (47x27mm)  make it 48x28
    ac_outlet_mounting_holes();
        
    
    }
}


module end_wall_drill_template()
difference(){
    union(){
    end_wall();
    translate([-5.5, width+1.5, 0])
        cube([14, 4, 62.5]);
        
    translate([-5.5, width-10, 0])
        cube([6, 14.5, 62.5]);
    }
    
    //subtract everything here
    translate([-10, -1, -15])
        cube([30, width-9, 80]);        // cut away end_wall
    translate([4, width+8, 50])
    rotate([90, 270, 0])
            cylinder(width+25, r1=1);   // end wall side hole
    translate([4, width+8, 62.5-50])
    rotate([90, 270, 0])
            cylinder(width+25, r1=1);   // end wall (lower) side hole
    translate([0, 229, 62.5-7])
        cube([30, 10, 7.1]);            // cut away end_wall
    translate([0, width-7.5, 0.5])
        cube([6, 9
    , 62.5]);
}


module end_wall_w_vent_holes()
// translate([0, 0, 10])        // this will raise the end_wall_w_vent_holes
    difference(){
        end_wall();
    for (i=[1 : 20]){
        for (j=[1 : 5]){
            translate([2.5, 15+i*10, 6 + j*10])
                rotate([90, 90, 270])
                    cylinder(h=10, r=2.5);
        }   // close j loop
    }   // close i loop
}       // close module end_wall_w_vent_holes()

// end_wall();
// enclosure_bottom();

//translate([5, width+4, 50])
//rotate([90, 270, 0])
            //cylinder(h=width+8., r1=2.1, r2=2.1);  // end wall side hole

module end_wall()       // this module slips over an 8.5mm end ribb
difference()
{
    union(){
        translate([-6+6, 8, 7]) 
            cube([9, width-15.5, 6.9]);     // bottom flap
        translate([-5.5, 0, 0])
            cube([5.5, width, 62.5]);       // back wall
        translate([-0.5, 8, 10])
            cube([14.5, 5, 62.5-10]);       // right flap
        translate([9.5, 5, 10])
            cube([4.5, 3, 62.5-10]);          // right 'hook'
        translate([-0.5, width-12.5, 10])
            cube([14.5, 5, 62.5-10]);       // left flap
        translate([9.5, width-7.5, 10])
            cube([4.5, 3, 62.5-10]);          // left 'hook'
        }
    union(){
        // end wall holes - bottom
        translate([5, 20, 6])
            cylinder(h=height+8., r1=1);    // end wall bottom hole
        translate([5, 120, 6])
            cylinder(h=height+8., r1=1);    // end wall bottom hole
        translate([5, 220, 6])
            cylinder(h=height+8., r1=1);    // end wall bottom hole
        translate([5, width+4, 50])
        rotate([90, 270, 0])
            cylinder(width+8, r1=1);        // end wall side hole
        }
}



module extension()
    translate([0, 7, 7])
    difference(){
    union()
    {
        translate([-12, 0.5, 0])
            cube([22, width-15, 6.9]);  // bottom flap
        
        // bottom extension
        difference(){
        translate([-100-shift, -7, -7])
            cube([extension_length, width, 7]);
            bottom_extension_cut();
        }
        
        // right wall extension
        translate([-100-shift, -7, -0])
            cube([extension_length, 7.5, 62.5-7]);
        // right wall overlap
        translate([-12, 0.5, -0])
            cube([27, 6, 62.5-7]);  // right flap
        // right wall slot close
        translate([10.5, -2.5, 0])
            cube([4.5, 3, 62.5-7]);
        
        // left wall extension
        translate([-100-shift, 240-14-0.5, -0])
            cube([extension_length, 7.5, 62.5-7]);
        // left wall overlap
        translate([-12, 240-17-3.5, -0])
            cube([27, 6, 62.5-7]);  // left flap
            // left wall slot close
        translate([10.5, 240-14-.5, 0])
            cube([4.5, 3, 62.5-7]);
        translate([-170-shift, -60, -7])
            union(){
                stepper_mount_1();
                stepper_mount_2();
                stepper_mount_3();
                stepper_mount_4();
            }
        }
    union(){
    // translate([-36.5, 225, 32.5])       // x was -40, -24.5
        // usb_conn_hole();
    
    translate([-92-shift, -4, 0])
        extension_wall_cut();
    translate([-92-shift, 220, -4])
        extension_wall_cut();
        
    translate([5, 20-7, -0.1])
        thread_formed_screw();
    translate([5, 120-7, -0.1])
        thread_formed_screw();
    translate([5, 220-7, -0.1])
        thread_formed_screw();
    translate([0, -0.1, 8])
        thread_formed_screw_y_plus();
    translate([0, -0.1, 43])
        thread_formed_screw_y_plus();
    translate([0, -6.9, 8])
        thread_formed_screw_y_left();
    translate([0, -6.9, 43])
        thread_formed_screw_y_left();
    translate([-170-shift, -60, -7])
        union(){
            stepper_hole_1();
            stepper_hole_2();
            stepper_hole_3();
            stepper_hole_4();
            }
        }
}




module uno_board_mount_bracket()
translate([-15, 255, -117])
rotate([90, 0, 270])
difference(){
union(){
    translate([18, 120, -3])
        union(){
        cube([78, 58, 6]);
        }
    uno_mount_1();
    uno_mount_2();
    uno_mount_3();
    uno_mount_4();
}

// we subract the holes below this line
union(){
    uno_hole_1();
    uno_hole_2();
    uno_hole_3();
    uno_hole_4();
    uno_bracket_holes();
    }
}



module uno_bracket_holes()
union(){
translate([uno_mount1_x + 25, uno_mount1_y, -4])
    cylinder(h=8, r1=1.5, r2=1.5);
translate([uno_mount1_x -12, uno_mount1_y + 30, -4])
    cylinder(h=8, r1=1.5, r2=1.5);
}

module usb_conn_hole()
    cube([12.5, 20, 13.5]);


module bottom_extension_cut()
    translate([-92-shift, 3, -4])  //-4 leaves 3mm thick bottom
        cube([shift+80, width-20, 8]);

module extension_wall_cut()
    cube([shift+80, 10, 65]);

module top_surface()
translate([0, 0, wall_height])
    cube(size=[length, width, height], center = false);


module thread_formed_screw()
        // screw cylinder is a 'cone' from r1 to r2 to ease screw installation.
    // rotate([90, 0, 0])
        cylinder(h=height+4.1, r1=1.65, r2=1.25);

module thread_formed_screw_y_plus()
rotate([270, 0, 0])
    translate([5, 0, 0])
        thread_formed_screw();

module thread_formed_screw_y_left()
rotate([90, 0, 0])
    translate([5, 0, -240+7])
        thread_formed_screw();

module enclosure_bottom()
{
    difference()
    {
        union()
        {
            // everything in here is added 
            cube(size=[length, width, height], center = false);
            ps_mount_1();
            ps_mount_2();
            ps_mount_3();
            ps_mount_4();
            
            uno_mount_1();
            uno_mount_2();
            uno_mount_3();
            uno_mount_4();
            
            stepper_mount_1();
            stepper_mount_2();
            stepper_mount_3();
            stepper_mount_4();
            right_wall();
            left_wall();
            right_wall_ribbs();
            left_wall_ribbs();
            bottom_ribbs();
            
        }
        
        union()
        {
             // everything in here is subtracted
            uno_hole_1();
            uno_hole_2();
            uno_hole_3();
            uno_hole_4();
            
            stepper_hole_1();
            stepper_hole_2();
            stepper_hole_3();
            stepper_hole_4();
            ps_hole_1();
            ps_hole_2();
            ps_hole_3();
            ps_hole_4();
            right_wall_holes();
            left_wall_holes();
            bottom_holes();

        }
    }
}


// my_mounting_hole();
// ps_mount_1();
// stepper_hole_1();
// bottom_hole_and_screwhead();
// klam_fillet_cylinder_ps_pad();
// right_wall_holes();
// bottom_holes();


module bottom_holes()
    union(){
        translate([5, -20, -0.1])
            bottom_hole();
        translate([5, -120, -0.1])
            bottom_hole();
        translate([5, -220, -0.1])
            bottom_hole();
        translate([245, -20, -0.1])
            bottom_hole();
        translate([245, -120, -0.1])
            bottom_hole();
        translate([245, -220, -0.1])
            bottom_hole();
    }
    

module bottom_hole()
    translate([0, 240, 0])
    rotate([270, 180, 90])
        right_wall_hole();


module left_wall_holes()
    union(){
        translate([5, 0.1, 15])
            left_wall_hole();
        translate([5, 0.1, 50])
            left_wall_hole();
        // translate([125, 0.1, 15])
            // left_wall_hole();        // lower center
        //translate([125, 0.1, 50])
            //left_wall_hole();         // upper center
        translate([245, 0.1, 15])
            left_wall_hole();
        translate([245, 0.1, 50])
            left_wall_hole();
    }



module left_wall_hole()
    translate([0, 240, 0])
    rotate([270, 90, 90])
        right_wall_hole();

module right_wall_holes()
    union(){
        translate([5, -0.1, 15])
            right_wall_hole();
        translate([5, -0.1, 50])
            right_wall_hole();
        // translate([125, -0.1, 15])
            // right_wall_hole();       // lower center
        // translate([125, -0.1, 50])
            // right_wall_hole();       // upper center
        translate([245, -0.1, 15])
            right_wall_hole();
        translate([245, -0.1, 50])
            right_wall_hole();
    }
    
    

module right_wall_hole()
    rotate([0, 90, 90])
    union(){
        // screw cylinder is a 'cone' from r1 to r2 to ease screw installation.
        cylinder(h=height+6.2, r1=2.1, r2=2.1);
        cylinder(h=2.1, r1=3, r2=3);
        }


module bottom_ribbs()
        union(){
            cube([10, width, 9]);       // origionally z = 7
            translate([240, 0, 0])
                cube([10, width, 7]);   // origionally z = 7
        }
     
        


module left_wall_ribbs()
    translate([0, width-7, 0])
        right_wall_ribbs();

module right_wall_ribbs()
    union(){
        translate([0, 0, 0])
            wall_ribb();
        translate([length/2-5, 0, 0])
            wall_ribb();
        translate([length-10, 0, 0])
            wall_ribb();
    }
    


module wall_ribb()
    cube([10, 7, 62.5]);


module right_wall()
    cube([length, height, wall_height]);

module left_wall()
translate([0, width - height, 0])
    cube([length, height, wall_height]);


/*
/ ********* Stepper Motor mounting pads *********
*/
module stepper_mount_1()
    translate([stepr_mt1_x, stepr_mt1_y, 3])
        klam_fillet_cylinder_ps_pad();

module stepper_mount_2()
    translate([stepr_mt1_x + 31, stepr_mt1_y + 31, 3])
        klam_fillet_cylinder_ps_pad();

module stepper_mount_3()
    translate([stepr_mt1_x + 31, stepr_mt1_y, 3])
        klam_fillet_cylinder_ps_pad();

module stepper_mount_4()
    translate([stepr_mt1_x, stepr_mt1_y + 31, 3])
        klam_fillet_cylinder_ps_pad();

/*
/ ********* Stepper Motor mounting holes *********
*/
module stepper_hole_1()
    translate([stepr_mt1_x, stepr_mt1_y, 0])
        bottom_hole_and_screwhead();

module stepper_hole_2()
    translate([stepr_mt1_x + 31, stepr_mt1_y + 31, 0])
        bottom_hole_and_screwhead();
        
module stepper_hole_3()
    translate([stepr_mt1_x + 31, stepr_mt1_y, 0])
        bottom_hole_and_screwhead();

module stepper_hole_4()
    translate([stepr_mt1_x, stepr_mt1_y + 31, 0])
        bottom_hole_and_screwhead();


/*
/ ********* Arduino Uno mounting pads *********
*/
module uno_mount_1()
    translate([uno_mount1_x, uno_mount1_y, 3])
        klam_fillet_cylinder_uno_pad();

module uno_mount_2()
    translate([uno_mount1_x + 52.07, uno_mount1_y + 33.2, 3])
        klam_fillet_cylinder_uno_pad();

module uno_mount_3()
    translate([uno_mount1_x + 52.07, uno_mount1_y + 5.08, 3])
        klam_fillet_cylinder_uno_pad();

module uno_mount_4()
    translate([uno_mount1_x + 1.27, uno_mount1_y + 48.26, 3])
        klam_fillet_cylinder_uno_pad();

module uno_hole_1()
    translate([uno_mount1_x, uno_mount1_y, 0])
        uno_hole();
        
module uno_hole_2()
    translate([uno_mount1_x + 52.07, uno_mount1_y + 33.2, 0])
        uno_hole();
        
module uno_hole_3()
    translate([uno_mount1_x + 52.07, uno_mount1_y + 5.08, 0])
        uno_hole();
        
module uno_hole_4()
    translate([uno_mount1_x + 1.27, uno_mount1_y + 48.26, 0])
        uno_hole();
        

module uno_hole()
    union(){
    translate([0, 0, -0.1])
        cylinder(h=height+4, r1=1.3, r2=1.3);
        // we add a 'pilot' cone to ease screwing in the uno,
    translate([0, 0, height+2])
        cylinder(h=1.1, r1=1.3, r2=1.7);
    }

/*
/ ********* Power Supply mounting pads *********
*/
module ps_mount_1()
    translate([ps_mount1_x, ps_mount1_y, height])
        klam_fillet_cylinder_ps_pad();

module ps_mount_2()
    translate([ps_mount1_x+120, ps_mount1_y, height])
        klam_fillet_cylinder_ps_pad();

module ps_mount_3()
    translate([ps_mount1_x+120, ps_mount1_y+80, height])
        klam_fillet_cylinder_ps_pad();

module ps_mount_4()
    translate([ps_mount1_x, ps_mount1_y+80, height])
        klam_fillet_cylinder_ps_pad();
        
/*
/ ********* Power Supply mounting holes *********
*/
module ps_hole_1()
    translate([ps_mount1_x, ps_mount1_y, 0])
        bottom_hole_and_screwhead();

module ps_hole_2()
    translate([ps_mount1_x+120, ps_mount1_y, 0])
        bottom_hole_and_screwhead();

module ps_hole_3()
    translate([ps_mount1_x+120, ps_mount1_y+80, 0])
        bottom_hole_and_screwhead();

module ps_hole_4()
    translate([ps_mount1_x, ps_mount1_y+80, 0])
        bottom_hole_and_screwhead();


module bottom_hole_and_screwhead()
    // we use this module for the stepper motor (35mm M3) and power supply (8mm M3).  Both are screwed from the bottom using M3 screws.
    // USE RADIUS r=1.75 FOR through-hole INSTALL OF M3 SCREW
// USE RADIUS r=3 FOR through-hole INSTALL 6mm SCREW-HEAD 
    translate([0, 0, -0.1])
        union(){
        // screw cylinder is a 'cone' from r1 to r2 to ease screw installation.
        cylinder(h=height+4.1, r1=2.1, r2=1.75);
        cylinder(h=4, r1=3, r2=3);
        }

            


module klam_fillet_cylinder_ps_pad(
    cylinder_height=height,
    cylinder_radius=5,
    fillet_radius_bottom=2,
    fillet_radius_top=0,
    nfaces=50
) {
    /* created by Kevin Lam on Dec 3, 2016 */
    union() {      
        cylinder(cylinder_height, r=cylinder_radius, $fn=nfaces, false);
        
        if (fillet_radius_bottom > 0) {
            difference() {
                cylinder(fillet_radius_bottom, r=cylinder_radius+fillet_radius_bottom, $fn=nfaces, false);
                translate([0, 0, fillet_radius_bottom])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_bottom, 0, 0])
                circle(fillet_radius_bottom, $fn=nfaces);
            }
        }
        
        if (fillet_radius_top>0) {
            difference() {
                translate([0,0,cylinder_height-fillet_radius_top])
                cylinder(fillet_radius_top, r=cylinder_radius+fillet_radius_top, $fn=nfaces, false);
                
                translate([0, 0, cylinder_height-fillet_radius_top])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_top, 0, 0])
                circle(fillet_radius_top, $fn=nfaces);
            }
        }
    }
}




module klam_fillet_cylinder_uno_pad(
    cylinder_height=height,
    cylinder_radius=3,
    fillet_radius_bottom=3,
    fillet_radius_top=0,
    nfaces=50
) {
    /* created by Kevin Lam on Dec 3, 2016 */
    union() {      
        cylinder(cylinder_height, r=cylinder_radius, $fn=nfaces, false);
        
        if (fillet_radius_bottom > 0) {
            difference() {
                cylinder(fillet_radius_bottom, r=cylinder_radius+fillet_radius_bottom, $fn=nfaces, false);
                translate([0, 0, fillet_radius_bottom])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_bottom, 0, 0])
                circle(fillet_radius_bottom, $fn=nfaces);
            }
        }
        
        if (fillet_radius_top>0) {
            difference() {
                translate([0,0,cylinder_height-fillet_radius_top])
                cylinder(fillet_radius_top, r=cylinder_radius+fillet_radius_top, $fn=nfaces, false);
                
                translate([0, 0, cylinder_height-fillet_radius_top])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_top, 0, 0])
                circle(fillet_radius_top, $fn=nfaces);
            }
        }
    }
}
