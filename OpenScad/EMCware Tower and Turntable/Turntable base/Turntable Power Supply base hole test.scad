// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad

// increase the visual detail
$fn = 100;

// my variables
length = 30;       // x
width = 15;        // y
height = 3;        // z


uno_mount1_x = 7;
uno_mount1_y = 7.5;

stepper_mount1_x = 22;
stepper_mount1_y = 7.5;


module complexObject()
{
    difference()
    {
        union()
        {
            // everything in here is added 
            cube(size=[length, width, height], center = false);
            // ps_mount_1();
            // uno_mount_1();
            uno_pad_1();
            stepper_mount_1();
        }
        
        union()
        {
             // everything in here is subtracted
            stepper_hole_and_screwhead1();
            uno_hole1();
            // ps_hole1();

        }
    }
}
complexObject();

// my_mounting_hole();
// ps_mount_1();
// stepper_hole_1();
// stepper_hole();
// uno_hole();


module stepper_mount_1()
    translate([stepper_mount1_x, stepper_mount1_y, 0])
        my_mounting_pad();


module stepper_hole_and_screwhead1()
    translate([stepper_mount1_x, stepper_mount1_y, 0])
        stepper_hole_and_screwhead();


        
module uno_pad_1()
    translate([uno_mount1_x, uno_mount1_y, 3])
        klam_fillet_cylinder_uno_pad();
        

module my_mounting_pad()
    cylinder(h=height+3, r1=4, r2=4);




// USE RADIUS r=1.6 FOR THREAD INSTALL OF M3 SCREW
module uno_hole1()
    translate([uno_mount1_x, uno_mount1_y, -0.1])
        uno_hole();
        
    
module uno_hole()
    union(){
    translate([0, 0, -0.1])
        cylinder(h=height+4, r1=1.6, r2=1.6);
        // we add a 'pilot' cone to ease screwing in the uno,
    translate([0, 0, height+2])
        cylinder(h=1.1, r1=1.6, r2=1.8);
    }
        

// USE RADIUS r=1.75 FOR through-hole INSTALL OF M3 SCREW
// USE RADIUS r=3 FOR through-hole INSTALL 6mm SCREW-HEAD 
module stepper_hole_and_screwhead()
    translate([0, 0, -0.1])
        union(){
        // screw cylinder is a 'cone' from r1 to r2 to ease screw installation.
        cylinder(h=height+4.1, r1=2.1, r2=1.75);
        cylinder(h=4, r1=3, r2=3);
        }




module klam_fillet_cylinder_uno_pad(
    cylinder_height=height,
    cylinder_radius=3,
    fillet_radius_bottom=3,
    fillet_radius_top=0,
    nfaces=50
) {
    /* created by Kevin Lam on Dec 3, 2016 */
    union() {      
        cylinder(cylinder_height, r=cylinder_radius, $fn=nfaces, false);
        
        if (fillet_radius_bottom > 0) {
            difference() {
                cylinder(fillet_radius_bottom, r=cylinder_radius+fillet_radius_bottom, $fn=nfaces, false);
                translate([0, 0, fillet_radius_bottom])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_bottom, 0, 0])
                circle(fillet_radius_bottom, $fn=nfaces);
            }
        }
        
        if (fillet_radius_top>0) {
            difference() {
                translate([0,0,cylinder_height-fillet_radius_top])
                cylinder(fillet_radius_top, r=cylinder_radius+fillet_radius_top, $fn=nfaces, false);
                
                translate([0, 0, cylinder_height-fillet_radius_top])
                rotate_extrude($fn=nfaces)
                translate([cylinder_radius+fillet_radius_top, 0, 0])
                circle(fillet_radius_top, $fn=nfaces);
            }
        }
    }
}
