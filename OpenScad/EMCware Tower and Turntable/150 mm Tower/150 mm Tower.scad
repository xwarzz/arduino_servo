// https://cubehero.com/2013/11/19/know-only-10-things-to-be-dangerous-in-openscad/
// https://www.thingiverse.com/jumpstart/openscad
// rotate([0, 0, 0])

/*
my template
module complexObject()
{
    difference()
    {
        union()
        {
             // everything in here is added
        }
        
        union()
        {
             // everything in here is subtracted
        }
    }
}
complexObject();
*/

    
// my variables
length = 152;           // x
width = 69;             // y
height = 80;            // z
tts_hole = 10;          // top_turn_screw_hole_radius
bts_hole1 = 5;          // bottom_turn_screw_hole1_radius
bts_hole2 = 12;         // bottom_turn_screw_hole2_radius
bearing1_x = 14;        // 14mm from bottom
bearingSpacing = 72;    // two x carriage bearing (upper and lower)

stepperSpacing = 31;     // nema 17 Stepper Motor


// increase the visual detail
$fn = 100;


module xCarriage()
{
    difference()
    {
        union()     // everything in here is added
        {
            cube(size=[length, width, height], center = false);
        }
        
        union()     // everything in here is subtracted
        {
            
            cut_away_cube();
            // bottom_corner(); // bottom back row round
            top_corner();
            bottom_slice();
            lower_rod();
            upper_rod();
            top_turn_screw_hole();
            bottom_turn_screw_holes();
            // bearing1();
            // bearing2();
            stepper_hole1();
            stepper_hole2();
            stepper_hole3();
            stepper_hole4();
            // leadScrewHole1();
            // leadScrewHole2();
            // leadScrewSpringHole();
            // leadScrewCouplerHole();
            // stopSwitchHole();
            // xyStops();  // this slice is 'added' to the 'cut_away_cube' module
            //xAxisStopSwitchHole1(); // added to the 'cut_away_cube' module
            // xAxisStopSwitchHole2(); // added to the 'cut_away_cube' module
            // xAxisStopSwitchHole3(); // added to the 'cut_away_cube' module
            // xAxisStopSwitchHole4(); // added to the 'cut_away_cube' module
            frontRound1();
            frontRound2();
            frontRound3();
            frontRound4();
            // leadScrewRectangle();
            zStopRectangle1();
            zStopRectangle2();
            zAxisStopSwitchHoleTop();
            zAxisStopSwitchHoleBottom();
            // xyStopWireSlot();
        }
    }
}
xCarriage();


// lower_rod();
// upper_rod();
// bottom_slice();
// side_corner();
// cut_away_cube();
// top_turn_screw_hole();
// bottom_turn_screw_holes();
// bearing1();
// bearing2();
// bottom_fillet();
// top_fillet();
// stepper_hole1();
// stepper_hole2();
// stepper_hole3();
// stepper_hole4();
// leadScrewHole1();
// xyStops();
// frontRound1();
// frontRound2();
// frontRound3();
// frontRound4();
// leadScrewHole2();
// leadScrewRectangle();
// zStopRectangle1();
// zStopRectangle2();
// zAxisStopSwitchHoleTop();
// zAxisStopSwitchHoleBottom();
// leadScrewSpringHole();
// xyStopWireSlot();
//  leadScrewCouplerHole();

module leadScrewCouplerHole(){
    translate([bearingSpacing/2 + bearing1_x, 14, 61])  // in line with bearing holes. 
       cylinder(h=20, r1 = 10, r2 = 10);
}

module xyStopWireSlot(){
    translate([-1,25,75])
        cube(size=[154,2,6], center = false);
}

module leadScrewSpringHole(){
    translate([bearingSpacing/2 + bearing1_x, 14, 30])  // in line with bearing holes. 
       cylinder(h=54, r1 = 8, r2 = 8);
}


module zAxisStopSwitchHoleBottom()
    translate([-141, -2.25, -15])
        // zAxisStopSwitchHole();
{
    translate([147.5, 49.5, height - 5])        // pivot slightly (147.5)
            cylinder(h=21, r1 = 1, r2 = 1);
    translate([147, 40, height - 5])
            cylinder(h=21, r1 = 1, r2 = 1);
}

module zAxisStopSwitchHoleTop()
    translate([0, 5.75, -15])
        // zAxisStopSwitchHole();
{
    translate([146.5, 49.5, height - 5])        // pivot slightly (146.5)
            cylinder(h=21, r1 = 1, r2 = 1);
    translate([147, 40, height - 5])
            cylinder(h=21, r1 = 1, r2 = 1);
}

//module zAxisStopSwitchHole(){
//    translate([146.5, 49.5, height - 5])
//            cylinder(h=21, r1 = 1, r2 = 1);
//    translate([147, 40, height - 5])
//            cylinder(h=21, r1 = 1, r2 = 1);
// }

module zStopRectangle2()
    translate([0, 31, 74])
        zStopRectangle();
        
module zStopRectangle1()
    translate([141, 39, 74])
        zStopRectangle();
        
module zStopRectangle(){
       cube([15, 23, 7]);
}


module leadScrewRectangle(){
    translate([39, 8.5, 27])  // in line with bearing holes. 
       cube([22.5, 11, 54]);
}

module leadScrewHole3(){
    translate([bearingSpacing/2 + bearing1_x, 14, 63])  // in line with bearing holes. 
       cylinder(h=18, r1 = 11, r2 = 11);
}

module leadScrewHole2(){
    translate([bearingSpacing/2 + bearing1_x, 14, -1])  // in line with bearing holes. 
       cylinder(h=10, r1 = 11, r2 = 11);
}


module leadScrewHole1(){
    translate([bearingSpacing/2 + bearing1_x, 14, -1])  // in line with bearing holes. 
       cylinder(h=82, r1 = 6, r2 = 6);
}



module frontRound4()
translate([140, 7, 0])
    frontRound3();


module frontRound3()
translate([0, -18, 62])
    rotate([270, 0, 0])
        frontRound();

module frontRound2()
translate([140, 7, 0])
    frontRound();

module frontRound1()
    frontRound();
 
module frontRound()
{
translate([20, 62, 80])
    rotate([0, 90, 180])
        difference() {
            cube(size=[10,10,30], center = false);
                translate([10, 10, 0])
                    cylinder(h = 30, r1 = 10, r2 = 10);
    }
}

module xAxisStopSwitchHole4()
    translate([-0, 0, -70])
        xAxisStopSwitchHole();
        
module xAxisStopSwitchHole3()
    translate([-9.5, 0, -70.5])    // we pivot slightly (z= -70.5) so finger sicks out more
        xAxisStopSwitchHole();
        
// module xAxisStopSwitchHole2()
    // translate([-9.5, 0, 0])
        // xAxisStopSwitchHole();

module xAxisStopSwitchHole2()
    translate([-9.5, 0, 0])
        xAxisStopSwitchHole();

module xAxisStopSwitchHole1()
    translate([0, 0, -0.5])     // we pivot slightly (z=-0.5) so finger sicks out more.
        xAxisStopSwitchHole();

module xAxisStopSwitchHole(){
    translate([115.75, 22, height - 5])
        rotate([90, 0, 0])
            cylinder(h=21, r1 = 1, r2 = 1);
}

module xyStops(){       // this slice is 'added' to the 'cut_away_cube' module
    translate([98,22,-1])
        cube(size=[25,7,85], center = false);
}


module stepper_hole4()
    translate([135, 62.5, 24.5])
        stepper_hole();

module stepper_hole3()
    translate([135, 62.5, 55.5])
        stepper_hole(); 

module stepper_hole2()
    translate([135, 31.5, 55.5])
        stepper_hole();

module stepper_hole1()
    translate([135, 31.5, 24.5])
        stepper_hole();

module stepper_hole(){
    rotate([0, 90, 0])
        cylinder(h=20, r1 = 1.75, r2 = 1.75);
    // we add another larger cylinder for the screw heads to rest in.
        rotate([0, 90, 0])
            cylinder(h=10, r1 = 3, r2 = 3);
}


module top_fillet()
    translate([132, 28, .01])
        difference() {
            // translate([142, 0, 0])
            cube(size=[10,10,80], center = false);
                translate([0, 10, -1])
                    cylinder(h = 90, r1 = 10, r2 = 10);
        }



module bottom_fillet() {
    translate([12, 28, -1])
        difference() {
            cube(size=[10,10,85], center = false);
                translate([10, 10, 0])
                    cylinder(h = 90, r1 = 10, r2 = 10);
    }
}


module bearing1(){
    translate([bearing1_x, 14, -1])
       cylinder(h=82, r1 = 9.5, r2 = 9.5);
}

module bearing2(){
    translate([bearingSpacing + bearing1_x, 14, -1])
       cylinder(h=82, r1 = 9.5, r2 = 9.5);
}


module top_turn_screw_hole(){
    translate([141, 47, 40])
        rotate([0, 90, 0])
            cylinder(h=12, r1 = tts_hole, r2 = tts_hole);
}

module bottom_turn_screw_holes(){
    translate([2, 47, 40])
        rotate([0, 90, 0])
            cylinder(h=15, r1 = bts_hole1, r2 = bts_hole1);
    // We add a 2nd parallel rod with 2mm radius that penetrates the bottom
    translate([10, 47, 40])
        rotate([0, 90, 0])
            cylinder(h=3, r1 = bts_hole2, r2 = bts_hole2);
}

module cut_away_cube(){
    difference() {
        union()
        {
            translate([12, 28, -1])
                cube(size=[130,45,82], center = false);
            // xyStops();
            xAxisStopSwitchHole1();
            xAxisStopSwitchHole2();
            xAxisStopSwitchHole3();
            xAxisStopSwitchHole4();
        }
        union()
        {
            bottom_fillet();
            top_fillet();
         }
    }
}

module upper_rod(){
    translate([2, 47, 60])
        rotate([0, 90, 0])
            cylinder(h=155, r1 = 4, r2 = 4);
    // We add a 2nd parallel rod with 2mm radius that penetrates the bottom
    translate([-1, 47, 60])
        rotate([0, 90, 0])
            cylinder(h=155, r1 = 2, r2 = 4);
}

module lower_rod(){
    translate([2, 47, 20])
        rotate([0, 90, 0])
            cylinder(h=155, r1 = 4, r2 = 4);
    // We add a 2nd parallel rod with 2mm radius that penetrates the bottom
    translate([-1, 47, 20]) 
        rotate([0, 90, 0])
            cylinder(h=155, r1 = 2, r2 = 4);
}

module bottom_slice(){
    translate([-1,62,-1])
        cube(size=[15,12,85], center = false);
}

module side_corner(){
    translate([-5,59,0])
        cube(size=[20,10,10], center = false);
}

module bottom_corner () {
difference() {
    cube(size=[10,10,85], center = false);
        translate([10, 10, 0])
            cylinder(h = 90, r1 = 10, r2 = 10);
    }
}

module top_corner () {
difference() {
    translate([142, 0, 0])
        cube(size=[10,10,85], center = false);
            translate([142, 10, 0])
                cylinder(h = 90, r1 = 10, r2 = 10);
}
}