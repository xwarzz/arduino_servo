/*
 * This will give you a class name SomeClass. This class has a private integer member, bar. 
 * You can set the value of bar through the set() method, 
 * and you can get the value of bar via the get() method.
 * 
 */

class SomeClass
{

  // Class Member Variables - these are initialized at startup (when we create a class instance).
  int MySpeed;          // We made up this variable (could be anything)
  float Height;

  // Constructor - creates an instance and initializes the member variable(s)
  public: SomeClass(int myspeed, float height)
  {
    // we set the value of our class member variables to whatever the user gives it at instantiation.
    MySpeed = myspeed;
    Height = height;
  }
    //  this function is called a 'getter'  
    int get_speed()  // this function is how we get the current value of our class variables
    {
        return (MySpeed);
    }

        float get_height()  // this function is how we get the current value of our class variables
    {
        return (Height);
    }
    
    // these functions are our 'setters'.  We call when we want to change the variable values.
    void set_speed( int new_speed )
    {
        MySpeed = new_speed;
    }

        void setter_height( float new_height )
    {
        Height = new_height;
    }
};


/*  We create two different instances of our class.
 *  Each class has the same variables but the value of the variables are different.
 *  
 */

SomeClass Instance1(100, 10.5);
SomeClass Instance2(200, 7.2);
SomeClass Instance3(300, 13.2);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  // Serial.println("the value of Instance1.get_speed is: " + Instance1.get_speed());
  Serial.print("the value of Instance1.get_speed is: ");
  Serial.println(Instance1.get_speed());
  Serial.print("the value of Instance2.get_speed is: ");
  Serial.println(Instance2.get_speed());
  Serial.print("the value of Instance3.get_speed is: ");
  Serial.println(Instance3.get_speed());
  Serial.println();
  Serial.print("the value of Instance1.get_speed is: ");
  Serial.println(Instance1.get_speed());
    Instance1.set_speed(5000);
  Serial.print("the value of Instance1.get_speed is: ");
  Serial.println(Instance1.get_speed());
  Serial.print("the value of Instance1.height is: ");
  Serial.println(Instance1.get_height());
  Serial.print("the value of Instance2.height is: ");
  Serial.println(Instance2.get_height());
  Serial.println();
  Serial.println("we have three class instances, with two variables per class or a total of 6 variables");
  Serial.println("we can very easily add more class instances without having to edit the class");
}

void loop() {
  // put your main code here, to run repeatedly:


}
